EAPI=5
inherit eutils qmake-utils

DESCRIPTION="Personal cookbook"
HOMEPAGE="https://openrecipes.jschwab.org"
SRC_URI="https://gitlab.com/ddorian/openrecipes/-/archive/v${PV}/openrecipes-v${PV}.tar.gz -> ${PF}.tar.gz"
RESTRICT="mirror"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="dev-qt/qtquickcontrols2
	dev-qt/qtnetwork:5
	dev-qt/qtxml:5
	dev-qt/qtsvg:5
	>=dev-libs/libsodium-1.0.12
	media-gfx/qrencode"

DEPEND="${RDEPEND}
	dev-qt/linguist-tools
	virtual/pkgconfig"

DOCS=( README.md )

S="${WORKDIR}/openrecipes-v${PV}"

src_configure() {
	eqmake5
}

src_compile() {
	emake
}

src_install() {
	einstall
	exeinto "/usr/bin/"
}

src_install() {
	emake STRIP="/bin/false" INSTALL_ROOT="${D}" install
	einstalldocs
}
