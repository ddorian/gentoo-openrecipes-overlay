Gentoo overlay for OpenRecipes
==============================

Add this overlay with:

	# layman -o https://gitlab.com/ddorian/gentoo-openrecipes-overlay/raw/master/gentoo-openrecipes-overlay.xml -f -a OpenRecipes
